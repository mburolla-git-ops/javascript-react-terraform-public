FROM node:14.18.0

# WORKDIR /app
# COPY . /app

RUN npm install react@18.2.0
RUN npm install react-scripts@5.0.1 -g --silent

# RUN npm run build
# RUN npm start
# CMD ["echo", "Built it!!!"]
