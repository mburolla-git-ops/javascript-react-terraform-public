# JavaScript React CI/CD Project
Builds and deploys a React web application to an AWS S3 bucket using a GitLab pipeline with Terraform.

# Getting Started
- Clone this repo
- Install and run Docker Desktop
- Add varialbes and their associated values to GitLab (Settings >> CI/CD):
  - AWS_ACCESS_KEY_ID
  - AWS_SECRET_ACCESS_KEY
  - AWS_DEFAULT_REGION
- Pipeline runs when changes are made to the `dev` branch
- Start live server: `npm start`

# Overview
![](./docs/cicd_overview.png)

# GitLab Pipeline Links
- [.gitlab-ci.yml Full Reference](https://docs.gitlab.com/ee/ci/yaml/index.html)
- [Predefined Variables Reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- [.gitlab-ci](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
- [CI/CD Variables](https://about.gitlab.com/blog/2021/04/09/demystifying-ci-cd-variables/)
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
- [Tutorials](https://docs.gitlab.com/ee/tutorials/)

# GitLab Runner

#### Download GitLab Runner Binary
- Open Powershell as admin
- Download and save runner binary in your Windows user directory
- `Invoke-WebRequest -Uri "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe" -OutFile "gitlab-runner.exe"`

#### Register the runner:
- `gitlab-runner.exe register`
  - Copy token from GitLab web console
  - Use the `docker` executor option
- `gitlab-runner.exe install`
- `gitlab-runner.exe start`
- `gitlab-runner.exe verify`
- GitLab Website
  - Make sure dot is green
  - Turn off shared runners slider

# Terraform
- `New-Alias -Name "tf" -Value "terraform"`
- [https://registry.terraform.io/](https://registry.terraform.io/)
- `terraform init -upgrade`  Sync up your changes with the state file in S3
- `tf destroy -var-file ./env-vars/dev.tfvars -auto-approve`
