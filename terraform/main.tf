terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.22.0"
    }
  }
}

provider "aws" {
  region = var.region
}

//
// Variables
//

variable "region" {}
variable "env_name" {}
variable "bucket_name" {}

//
// Resources
//

resource "aws_s3_bucket" "s3-react-bucket" {
  bucket        = format("%s-%s", var.env_name, var.bucket_name)
  force_destroy = true
}

resource "aws_s3_bucket_public_access_block" "example" {
  bucket              = aws_s3_bucket.s3-react-bucket.id
  block_public_acls   = false
  block_public_policy = false
}

resource "aws_s3_bucket_website_configuration" "s3-react-bucket" {
  bucket = aws_s3_bucket.s3-react-bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

resource "aws_s3_bucket_policy" "allow_web_access" {
  bucket = aws_s3_bucket.s3-react-bucket.id
  policy = data.aws_iam_policy_document.web-policy.json
}

resource "local_file" "deploy_script" {
  filename = "deploy.sh"
  content  = "aws s3 sync ./build s3://${var.env_name}-${var.bucket_name}"
}

//
// Data
//

data "aws_s3_bucket" "selected" {
  bucket = "dev-siu-react-web-app"
  depends_on = [
    aws_s3_bucket.s3-react-bucket
  ]
}

data "aws_iam_policy_document" "web-policy" {
  statement {
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]
    resources = [
      aws_s3_bucket.s3-react-bucket.arn,
      "${aws_s3_bucket.s3-react-bucket.arn}/*",
    ]
  }
}
